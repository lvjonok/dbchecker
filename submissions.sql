create table submissions(
  id          INTEGER PRIMARY KEY AUTOINCREMENT,
  chat_id     INTEGER,
  score1      REAL,
  score2      REAL,
  score3      REAL,
  query       TEXT,
  updated_at  INTEGER
);
package datastruct

import "sync"

type SafeIntSlice struct {
	sync.RWMutex
	ints []int64
}

func (s *SafeIntSlice) Add(num int64) {
	defer s.Unlock()

	s.Lock()
	s.ints = append(s.ints, num)
}

func (s *SafeIntSlice) Len() int {
	defer s.RUnlock()

	s.RLock()
	return len(s.ints)
}

func (s *SafeIntSlice) Remove(num int64) {
	defer s.Unlock()
	s.Lock()

	idx := 0
	for idx, _ := range s.ints {
		if s.ints[idx] == num {
			break
		}
	}

	s.ints[idx] = s.ints[len(s.ints)-1]
	s.ints = s.ints[:len(s.ints)-1]
}

func (s *SafeIntSlice) Contains(num int64) bool {
	defer s.RUnlock()

	s.RLock()
	for _, v := range s.ints {
		if v == num {
			return true
		}
	}

	return false
}

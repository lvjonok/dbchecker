package dbwrapper

import (
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

type IndexType string

const (
	BTree IndexType = "btree" // equality and range queries on data that can be sorted into some ordering
	Hash  IndexType = "hash"  // simple equality comparisons
	GIN   IndexType = "gin"   // inverted indices which are appropriate for data values that contain multiple component values, such as arrays
	BRIN  IndexType = "brin"  // most effective for columns whose values are well-correlated with the physical order of the table rows
)

func ParseIndexType(raw string) (IndexType, error) {
	switch raw {
	case "btree":
		return BTree, nil
	case "hash":
		return Hash, nil
	case "gin":
		return GIN, nil
	case "brin":
		return BRIN, nil
	default:
		return "", errors.Errorf("I do not know this index: %v", raw)
	}
}

type Indexable interface {
	Query() string
	Undo() string
}

type Index struct {
	Table      string
	Attributes []string
	Type       IndexType
}

func ParseIndex(raw string) (*Index, error) {
	re := regexp.MustCompile(`create index ([a-z_0-9]*) on ([a-z_0-9]*) using ([a-z_0-9]*) \(([a-z_0-9,\s]*)\);`)
	res := re.FindAllStringSubmatch(strings.ToLower(raw), -1)

	if len(res) != 1 {
		return nil, errors.Errorf("Cannot parse your index: %v", raw)
	}

	if len(res[0]) != 5 {
		return nil, errors.Errorf("Your query: '%s' does not seem correct", raw)
	}

	idx, err := ParseIndexType(strings.ToLower(res[0][3]))
	if err != nil {
		return nil, errors.Errorf("Failed to parse your index type, err: %v", err)
	}

	attrsRaw := strings.Split(res[0][4], ",")
	attrsTrim := []string{}

	for _, attr := range attrsRaw {
		attrsTrim = append(attrsTrim, strings.TrimSpace(attr))
	}

	return &Index{Table: res[0][2], Attributes: attrsTrim, Type: idx}, nil
}

func (idx Index) getIdxName() string {
	return strings.Join([]string{idx.Table, string(idx.Type), strings.Join(idx.Attributes, "_"), "idx"}, "_")
}

// Query creates sql query used to create this index
func (idx Index) Query() string {
	joinedAttrs := strings.Join(idx.Attributes, ", ")

	return "CREATE INDEX " + idx.getIdxName() + " ON " + idx.Table + " USING " + string(idx.Type) + " (" + joinedAttrs + ");"
}

// Undo creates sql query used to delete this index
func (idx Index) Undo() string {
	return "DROP INDEX " + idx.getIdxName() + ";"
}

type RawIndex struct {
	Doq   string
	Undoq string
}

func (idx RawIndex) Query() string {
	return idx.Doq
}

func (idx RawIndex) Undo() string {
	return idx.Undoq
}

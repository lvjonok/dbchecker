FROM postgres:latest

COPY dvdrental.tar dvdrental.tar
COPY dvdrental_heavy.tar dvdrental_heavy.tar
COPY setup.sh setup.sh

# TODO add creation for databases not by hand
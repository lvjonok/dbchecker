package tgwrapper

type StickerReaction string

const (
	Best    StickerReaction = "CAACAgIAAxkBAAIEKWJcIeCcLzVxbcCCm4BnKID4JwN1AAJ8EQACTbD4SGTMci63kLrWJAQ"
	Good    StickerReaction = "CAACAgIAAxkBAAIELWJcIh9ciRqY9QRQyZoAAakboEMJBwACog8AAiFQ-Ehdi-Xqd2-fhCQE"
	Bad     StickerReaction = "CAACAgIAAxkBAAIELmJcIipfzZag13YEAcU2eND-JNx6AAJcFQACZHv4SLbBi8piD9mDJAQ"
	Worst   StickerReaction = "CAACAgIAAxkBAAIEL2JcIitiUBgTGmBEgbcE24MDUjk5AALmEAACKNEBSR6vCL0PVAAB7iQE"
	Pooped  StickerReaction = "CAACAgIAAxkBAAIEUmJcJSbuWoYGgDFYKrqCcFTzpuQpAAKnFQACIDf5SESM_Lcfz8jwJAQ"
	Unknown StickerReaction = "CAACAgIAAxkBAAIEPGJcI0o_YTEC-B7XPVZtQpp2HoqcAAIPFAAC4GL4SECjr-bEFCTCJAQ"
	Error   StickerReaction = "CAACAgIAAxkBAAIEMGJcIj7iDPQAAUrEUld0e3fceDz6tgACEBQAAsbT-Eg-fzwgXJZmQyQE"
)

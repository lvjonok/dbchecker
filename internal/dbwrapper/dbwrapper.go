package dbwrapper

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
)

type Client struct {
	DB  *pgx.Conn
	Ctx context.Context
}

func NewClient(dbName string) (*Client, error) {
	urlLocal := fmt.Sprintf("postgres://postgres:1234@localhost:5432/%s", dbName)
	ctx := context.Background()
	conn, err := pgx.Connect(ctx, urlLocal)
	if err != nil {
		return nil, errors.Errorf("Failed to connect to database: %v", err)
	}

	return &Client{
		DB:  conn,
		Ctx: ctx,
	}, nil
}

func (c Client) TestPerformance(indices []Indexable) ([]*QueryTestResult, error) {
	defer c.undoIndices(indices)

	if err := c.applyIndices(indices); err != nil {
		return nil, errors.Errorf("failed to apply index, err: %v", err)
	}

	var result []*QueryTestResult

	for _, query := range TestingQueries {
		testRes, err := c.testQuery(query)
		if err != nil {
			return nil, err
		}

		result = append(result, testRes)
		fmt.Println(testRes.ExecInfo)
	}

	return result, nil
}

func (c Client) applyIndices(indices []Indexable) error {
	for _, index := range indices {
		if _, err := c.DB.Exec(c.Ctx, index.Query()); err != nil {
			return err
		}

	}

	return nil
}

func (c Client) undoIndices(indices []Indexable) error {
	for _, index := range indices {
		if _, err := c.DB.Exec(c.Ctx, index.Undo()); err != nil {
			return err
		}

	}

	return nil
}

func (c Client) testQuery(query string) (*QueryTestResult, error) {
	rows, err := c.DB.Query(c.Ctx, query)
	if err != nil {
		return nil, fmt.Errorf("failed to test query %v, err: %v", query, err)
	}

	var result string
	first_row := ""

	for rows.Next() {
		var row string
		err := rows.Scan(&row)
		if err != nil {
			return nil, fmt.Errorf("failed to parse row, err: %v", err)
		}

		result += row + "\n"
		if first_row == "" {
			first_row = row
		}
	}
	// last_row is basically: Execution Time: 14667.488 ms
	// thus for simplicity I just parse it from spaces

	temp := strings.Split(strings.Split(first_row, "cost=")[1], " rows")[0]

	cost, err := strconv.ParseFloat(strings.Split(temp, "..")[1], 64)
	if err != nil {
		return nil, fmt.Errorf("failed to parse cost from first row %v, err: %v", first_row, err)
	}

	return &QueryTestResult{ExecCost: cost, ExecInfo: result}, nil
}

package main

import (
	"log"

	"os"
	"os/signal"

	"github.com/m/v2/dbchecker/internal/dbwrapper"
	"github.com/m/v2/dbchecker/internal/notdb"
	tgwrapper "github.com/m/v2/dbchecker/internal/tgwrapper"
)

func main() {
	notdbu, err := notdb.NewNotdb("submissions.db")
	if err != nil {
		log.Fatal(err)
	}
	defer notdbu.Close()

	notdbh, err := notdb.NewNotdb("submissions_heavy.db")
	if err != nil {
		log.Fatal(err)
	}
	defer notdbh.Close()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			notdbu.Close()
			notdbh.Close()
			os.Exit(1)
		}
	}()

	botCtx := tgwrapper.NewBot(os.Getenv("APIKEY"))

	// message dinar
	// botCtx.SendSticker(260542648, tgwrapper.Best)

	dbClient, err := dbwrapper.NewClient("dvdrental")
	if err != nil {
		log.Fatal(err)
	}

	dbClientHeavy, err := dbwrapper.NewClient("dvdrental_20k")
	if err != nil {
		log.Fatal(err)
	}

	// submit handler
	doneSubmissions := make(chan bool)
	go botCtx.HandleSubmissions(&botCtx.Submissions, dbClient, notdbu, doneSubmissions)

	// TODO add implementation for many workers
	go botCtx.HandleSubmissions(&botCtx.SubmissionsHeavy, dbClientHeavy, notdbh, doneSubmissions)

	// messages handler
	doneMessages := make(chan bool)
	go botCtx.HandleMessages(notdbu, notdbh, doneMessages)

	<-doneSubmissions
	<-doneMessages
}

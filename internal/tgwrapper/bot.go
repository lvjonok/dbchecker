package tgwrapper

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/m/v2/dbchecker/internal/dbwrapper"
	"github.com/m/v2/dbchecker/internal/notdb"
)

const greetMsg = `Hi!
I will check your databases assignment solution and create rating  among all students.

Send me something like this (your submitted sql should be in the same format):

/submit CREATE INDEX idx1 ON customer USING hash (customer_id);
CREATE INDEX idx2 ON customer USING hash (active);`

type BotContext struct {
	Bot              *tgbotapi.BotAPI
	Logger           *log.Logger
	Updates          tgbotapi.UpdatesChannel
	Submissions      chan Submit
	SubmissionsHeavy chan Submit
	Queue            QueueOrder
}

func NewBot(apiKey string) *BotContext {
	bot, err := tgbotapi.NewBotAPI(apiKey)
	if err != nil {
		log.Panic(err)
	}
	bot.Debug = false
	log.Printf("Authorized on account %s\n", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	return &BotContext{
		Bot:              bot,
		Logger:           log.New(os.Stdout, "Context", log.Flags()),
		Updates:          bot.GetUpdatesChan(u),
		Submissions:      make(chan Submit, 100),
		SubmissionsHeavy: make(chan Submit, 100),
		Queue:            NewQueueOrder(),
	}
}

func (ctx *BotContext) SendMessage(chatId int64, message string) error {
	_, res := (*ctx).Bot.Send(tgbotapi.NewMessage(chatId, message))

	return res
}

func (ctx *BotContext) SendSticker(chatId int64, sticker StickerReaction) error {
	_, res := (*ctx).Bot.Send(tgbotapi.NewSticker(chatId, tgbotapi.FileID(sticker)))

	return res
}

func (ctx *BotContext) HandleSubmissions(Reader *chan Submit, Worker *dbwrapper.Client, Saver *notdb.Notdb, done chan bool) {
	for submission := range *Reader {
		// get currently running id
		ctx.Queue.Run(submission.Id)

		ctx.Logger.Printf("we run submission %v\n%v\n", submission.Id, submission.Indices)

		ctx.SendMessage(submission.ChatId, "I started processing your query")
		subRes, err := Worker.TestPerformance(submission.Indices)
		if err != nil {
			ctx.Logger.Printf("Error: %v\n", err)
			ctx.SendMessage(submission.ChatId, "Got error while testing: "+err.Error())
		} else {
			costs := []float64{}
			costOutput := ""
			for idx, res := range subRes {
				costOutput += fmt.Sprintf("%d - %.2f\n", idx, res.ExecCost)
				costs = append(costs, res.ExecCost)
			}

			err = Saver.Add(submission.ChatId, submission.raw, costs)
			if err != nil {
				ctx.Logger.Printf("error adding to saver: %v", err)
			}

			ctx.SendMessage(submission.ChatId,
				fmt.Sprintf("Finished your submission: \n%v", costOutput))

			ctx.handleRate(Saver, submission.ChatId)

			err = ioutil.WriteFile(fmt.Sprintf("chat-%v.txt", submission.ChatId), []byte(subRes[0].ExecInfo+"\n\n"+subRes[1].ExecInfo+"\n\n"+subRes[2].ExecInfo), 0644)
			if err != nil {
				ctx.Logger.Printf("failed to write data to file: %v", err)
			} else {
				// sending output
				file := tgbotapi.FilePath(fmt.Sprintf("chat-%v.txt", submission.ChatId))
				doc := tgbotapi.NewDocument(submission.ChatId, file)
				ctx.Bot.Send(doc)
			}

		}

		ctx.Queue.Finish(submission.Id)
	}

	done <- true
}

func (ctx *BotContext) handleRate(Saver *notdb.Notdb, chatId int64) {
	userPos, totalUsers, _ := Saver.Rate(chatId)

	var ratedPercent float64
	if totalUsers == 1 {
		ratedPercent = 1.0
	} else {
		ratedPercent = (float64(totalUsers) - 1 - float64(userPos)) / (float64(totalUsers) - 1)
	}

	ctx.SendMessage(chatId,
		fmt.Sprintf("You're top %.2f%%!\n%d among %d", ratedPercent*100.0, userPos+1, totalUsers))

	if ratedPercent > 0.7 {
		ctx.SendSticker(chatId, Best)
	} else if ratedPercent > 0.3 {
		ctx.SendSticker(chatId, Good)
	} else if ratedPercent > 0.05 {
		ctx.SendSticker(chatId, Bad)
	} else {
		ctx.SendSticker(chatId, Worst)
	}
}

func (ctx *BotContext) HandleMessages(Saver *notdb.Notdb, Saver2 *notdb.Notdb, done chan bool) {
	for update := range ctx.Updates {
		chatId := update.Message.Chat.ID
		ctx.Logger.Printf("msg: %v\n", update.Message)
		// if update.Message != nil && update.Message.Sticker != nil {
		// 	ctx.Logger.Printf("Sticker id %v\n", update.Message.Sticker.FileID)
		// 	continue
		// }

		if update.Message == nil || !update.Message.IsCommand() {
			if update.Message != nil {
				ctx.SendSticker(chatId, Unknown)
			}
			continue
		}
		var err error

		switch update.Message.Command() {
		case "start":
			fallthrough
		case "help":
			err = ctx.SendMessage(chatId, greetMsg)
		case "rating":
			ctx.handleRate(Saver, chatId)
		case "submit":
			if ctx.Queue.IsPresent(chatId) {
				ctx.SendMessage(chatId, "You are already running!")
				continue
			}

			submission, parseErr := NewSubmit(ctx, chatId, update.Message.CommandArguments())
			if parseErr != nil {
				ctx.SendMessage(chatId, "Failed: "+parseErr.Error())
				ctx.SendSticker(chatId, Pooped)
				continue
			}

			waitLen := ctx.Queue.Len()
			ctx.Queue.Add(submission.Id, submission)
			err = ctx.SendMessage(chatId,
				fmt.Sprintf("You submitted your work here. Queue len: %d", waitLen))

			ctx.Submissions <- *submission
		case "submit2":
			if ctx.Queue.IsPresent(chatId) {
				ctx.SendMessage(chatId, "You are already running!")
				continue
			}

			submission, parseErr := NewSubmit(ctx, chatId, update.Message.CommandArguments())
			if parseErr != nil {
				ctx.SendMessage(chatId, "Failed: "+parseErr.Error())
				ctx.SendSticker(chatId, Pooped)
				continue
			}

			waitLen := ctx.Queue.Len()
			ctx.Queue.Add(submission.Id, submission)
			err = ctx.SendMessage(chatId,
				fmt.Sprintf("You submitted your work here. Queue len: %d", waitLen))

			ctx.SubmissionsHeavy <- *submission
		default:
			err = ctx.SendMessage(chatId, "This command is unknown")
			ctx.SendSticker(chatId, Error)
		}

		if err != nil {
			ctx.Logger.Fatalf("fatal error : %v\n", err)
		}
	}

	done <- true
}

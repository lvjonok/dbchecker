package notdb

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

/*
create table submissions(
  id          INTEGER PRIMARY KEY AUTOINCREMENT,
  chat_id     INTEGER,
  score1      REAL,
  score2      REAL,
  score3      REAL,
  query       TEXT,
  updated_at  INTEGER
);
*/

type SubmissionDb struct {
	Id        int64
	ChatId    int64
	Score1    float64
	Score2    float64
	Score3    float64
	Query     string
	UpdatedAt int64 // from epoch 0 unix
}

type Notdb struct {
	DB *sql.DB
}

func NewNotdb(name string) (*Notdb, error) {
	db, err := sql.Open("sqlite3", name)
	if err != nil {
		return nil, err
	}

	return &Notdb{DB: db}, nil
}

func (d *Notdb) Close() error {
	return d.DB.Close()
}

// getChatIdSum returns the top result sum of specific chatId
func (d *Notdb) getChatIdSum(ChatId int64) (float64, error) {
	query := `SELECT (score1 + score2 + score3) from submissions 
						where chat_id=$1 order by (score1 + score2 + score3) limit 1`

	row := d.DB.QueryRow(query, ChatId)
	if row == nil {
		return 0, fmt.Errorf("failed to query chat id: %v", ChatId)
	}

	result := 0.0
	row.Scan(&result)

	return result, nil
}

func (d *Notdb) Add(ChatId int64, Raw string, Metrics []float64) error {
	query := `INSERT INTO submissions (chat_id, score1, score2, score3, query, updated_at)
		values ($1, $2, $3, $4, $5, $6)`

	_, err := d.DB.Exec(query, ChatId, Metrics[0], Metrics[1], Metrics[2], Raw, time.Now().Unix())
	if err != nil {
		return err
	}

	return nil
}

// Rate returns three entities, your idx in the whole rating, the number of unique users and possible error
func (d *Notdb) Rate(ChatId int64) (int, int, error) {
	type ChatScoreMapping struct {
		ChatId int64
		Score  float64
	}

	var results []ChatScoreMapping

	query := `select chat_id, (score1 + score2 + score3) from submissions 
						group by chat_id 
						having min(score1 + score2 + score3) 
						order by (score1 + score2 + score3);
						`

	rows, err := d.DB.Query(query)
	if err != nil {
		return 0, 0, fmt.Errorf("failed to query rating: %v", err)
	}

	for rows.Next() {
		mapping := ChatScoreMapping{}

		err := rows.Scan(&mapping.ChatId, &mapping.Score)
		if err != nil {
			return 0, 0, fmt.Errorf("failed to parse: %v", err)
		}

		results = append(results, mapping)
	}

	for idx, v := range results {
		if v.ChatId == ChatId {
			return idx, len(results), nil
		}
	}

	return len(results) - 1, len(results), nil
}

func (d *Notdb) UniqueUsers() ([]int64, error) {
	users := []int64{}

	query := `select distinct chat_id from submissions`
	rows, err := d.DB.Query(query)
	if err != nil {
		return nil, fmt.Errorf("failed to query unique users: %v", err)
	}

	for rows.Next() {
		var chatId int64
		err := rows.Scan(&chatId)
		if err != nil {
			return nil, fmt.Errorf("failed to scan, %v", err)
		}

		users = append(users, chatId)
	}

	return users, nil
}

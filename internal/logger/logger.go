package logger

import (
	"encoding/json"
	"os"
	"time"

	db "github.com/m/v2/dbchecker/internal/dbwrapper"
	"github.com/pkg/errors"
)

type LoggerMessage struct {
	Time    time.Time             `json:"time"`
	Indices []db.Indexable        `json:"indices"`
	Results []*db.QueryTestResult `json:"result"`
}

type Logger struct {
	file *os.File
}

func NewLogger() (*Logger, error) {
	f, err := os.OpenFile("info.log", os.O_RDWR|os.O_APPEND, 0666)
	if err != nil {
		return nil, errors.Errorf("failed to create logger, err: %v", err)
	}

	return &Logger{file: f}, nil
}

func (l Logger) Log(msg LoggerMessage) error {
	bytes, err := json.Marshal(msg)
	if err != nil {
		return errors.Errorf("failed to marshal json, err: %v", err)
	}

	_, err = l.file.WriteString(string(bytes) + "\n")
	if err != nil {
		return errors.Errorf("failed to log data, err: %v", err)
	}

	return nil
}

func (l Logger) Close() error {
	return l.file.Close()
}

#!/bin/bash

. secret.sh

until go run cmd/tgclient/main.go >> output.log; do
    echo "Bot crashed, restarting"
    date +%Y-%m-%d-%H-%M-%S
    sleep 1
done
package tgwrapper

import (
	"sync"

	ds "github.com/m/v2/dbchecker/internal/datastruct"
	"github.com/pkg/errors"
)

// QueueOrder saves id for each user and its id in submissions
type QueueOrder struct {
	lastId  ds.SafeInt      // id of the last submission accepted in queue
	running ds.SafeIntSlice // ids of submissions that are running
	waiting ds.SafeIntSlice // ids of submissions in the queue

	mapLocker   sync.RWMutex
	submissions map[int64]*Submit
}

func NewQueueOrder() QueueOrder {
	return QueueOrder{submissions: make(map[int64]*Submit)}
}

// Run adds submissionId to currently running queries
func (q *QueueOrder) Run(id int64) {
	q.waiting.Remove(id)

	q.running.Add(id)
}

func (q *QueueOrder) Finish(id int64) error {
	q.mapLocker.Lock()
	delete(q.submissions, id)
	q.mapLocker.Unlock()

	if q.running.Contains(id) {
		q.running.Remove(id)
		return nil
	}

	return errors.Errorf("Failed to finish because %d is not present", id)
}

// Add returns the order number for id
func (q *QueueOrder) Add(id int64, sub *Submit) int {
	q.mapLocker.Lock()
	q.submissions[id] = sub
	q.mapLocker.Unlock()

	q.waiting.Add(id)
	return q.waiting.Len()
}

func (q *QueueOrder) IsPresent(ChatId int64) bool {
	defer q.mapLocker.RUnlock()
	q.mapLocker.RLock()

	for _, v := range q.submissions {
		if v.ChatId == ChatId {
			return true
		}
	}

	return false
}

func (q *QueueOrder) Len() int {
	return q.waiting.Len()
}

func (q *QueueOrder) GenId() int64 {
	return q.lastId.Incr()
}

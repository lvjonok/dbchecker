package tgwrapper

import (
	"strings"

	"github.com/m/v2/dbchecker/internal/dbwrapper"
)

type Submit struct {
	Id      int64
	ChatId  int64
	Indices []dbwrapper.Indexable
	raw     string
}

func NewSubmit(ctx *BotContext, ChatId int64, rawQuery string) (*Submit, error) {
	indicesRaw := strings.Split(rawQuery, "\n")
	indices := []dbwrapper.Indexable{}
	for _, indexRaw := range indicesRaw {
		res, err := dbwrapper.ParseIndex(indexRaw)
		if err != nil {
			return nil, err
		}

		indices = append(indices, *res)
	}

	return &Submit{Id: (*ctx).Queue.GenId(), ChatId: ChatId, Indices: indices, raw: rawQuery}, nil
}

package datastruct

import "sync"

type SafeInt struct {
	sync.RWMutex
	i int64
}

func (s *SafeInt) Incr() int64 {
	defer s.Unlock()

	s.Lock()
	copy := s.i
	s.i = copy + 1

	return copy + 1
}

func (s *SafeInt) Save(num int64) {
	defer s.Unlock()

	s.Lock()
	s.i = num
}

func (s *SafeInt) Load() int64 {
	s.RLock()
	copy := s.i
	s.RUnlock()

	return copy
}

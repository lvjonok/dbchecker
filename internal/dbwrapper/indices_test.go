package dbwrapper_test

import (
	"log"
	"os"
	"testing"

	db "github.com/m/v2/dbchecker/internal/dbwrapper"
	"github.com/stretchr/testify/require"
)

func Test_indices(t *testing.T) {
	for _, query := range []string{
		"CREATE INDEX customer_hash_customer_id_idx ON customer USING hash (customer_id);",
		"CREATE INDEX customer_hash_customer_id_idx ON customer USING hash (customer_id,   asdf   );",
	} {
		res, err := db.ParseIndex(query)
		require.NoError(t, err)

		log := log.New(os.Stdout, "", log.Flags())
		log.Printf("%v", res)
	}
}
